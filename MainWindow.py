# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'UI.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import image_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 674)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QSize(800, 674))
        MainWindow.setMaximumSize(QSize(800, 674))
        icon = QIcon()
        icon.addFile(u":/pictures/Logo.png", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.actionhow_to_use = QAction(MainWindow)
        self.actionhow_to_use.setObjectName(u"actionhow_to_use")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.btnTransform = QPushButton(self.centralwidget)
        self.btnTransform.setObjectName(u"btnTransform")
        self.btnTransform.setGeometry(QRect(560, 520, 101, 41))
        font = QFont()
        font.setFamily(u"\u9ed1\u4f53")
        font.setPointSize(14)
        self.btnTransform.setFont(font)
        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(600, 70, 161, 51))
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(560, 120, 241, 241))
        self.label_7.setPixmap(QPixmap(u":/pictures/Edtshare\u4e8c\u7ef4\u7801.bmp"))
        self.label_8 = QLabel(self.centralwidget)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(570, 380, 221, 121))
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setEnabled(True)
        self.groupBox.setGeometry(QRect(20, 30, 531, 161))
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy1)
        font1 = QFont()
        font1.setFamily(u"\u534e\u6587\u7ec6\u9ed1")
        font1.setBold(False)
        font1.setWeight(50)
        self.groupBox.setFont(font1)
        self.groupBox.setAlignment(Qt.AlignCenter)
        self.groupBox.setFlat(False)
        self.groupBox.setCheckable(False)
        self.gridLayoutWidget_3 = QWidget(self.groupBox)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(20, 20, 491, 111))
        self.gridLayout_3 = QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.ToPPT = QRadioButton(self.gridLayoutWidget_3)
        self.ToPPT.setObjectName(u"ToPPT")

        self.gridLayout_3.addWidget(self.ToPPT, 1, 0, 1, 1)

        self.GetAudio = QRadioButton(self.gridLayoutWidget_3)
        self.GetAudio.setObjectName(u"GetAudio")

        self.gridLayout_3.addWidget(self.GetAudio, 1, 1, 1, 1)

        self.ToMP4 = QRadioButton(self.gridLayoutWidget_3)
        self.ToMP4.setObjectName(u"ToMP4")

        self.gridLayout_3.addWidget(self.ToMP4, 0, 0, 1, 1)

        self.ToCD = QRadioButton(self.gridLayoutWidget_3)
        self.ToCD.setObjectName(u"ToCD")

        self.gridLayout_3.addWidget(self.ToCD, 0, 1, 1, 1)

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setGeometry(QRect(20, 210, 531, 131))
        self.groupBox_2.setAlignment(Qt.AlignCenter)
        self.gridLayoutWidget = QWidget(self.groupBox_2)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(20, 20, 491, 101))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.DestPath = QLineEdit(self.gridLayoutWidget)
        self.DestPath.setObjectName(u"DestPath")

        self.gridLayout.addWidget(self.DestPath, 1, 1, 1, 1)

        self.label = QLabel(self.gridLayoutWidget)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.SourcePath = QLineEdit(self.gridLayoutWidget)
        self.SourcePath.setObjectName(u"SourcePath")

        self.gridLayout.addWidget(self.SourcePath, 0, 1, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.SourceButton = QPushButton(self.gridLayoutWidget)
        self.SourceButton.setObjectName(u"SourceButton")

        self.gridLayout.addWidget(self.SourceButton, 0, 2, 1, 1)

        self.DestButton = QPushButton(self.gridLayoutWidget)
        self.DestButton.setObjectName(u"DestButton")

        self.gridLayout.addWidget(self.DestButton, 1, 2, 1, 1)

        self.groupBox_3 = QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setGeometry(QRect(20, 390, 531, 201))
        self.groupBox_3.setAlignment(Qt.AlignCenter)
        self.txtOutput = QTextBrowser(self.groupBox_3)
        self.txtOutput.setObjectName(u"txtOutput")
        self.txtOutput.setGeometry(QRect(20, 20, 491, 161))
        self.checkUpdate = QPushButton(self.centralwidget)
        self.checkUpdate.setObjectName(u"checkUpdate")
        self.checkUpdate.setGeometry(QRect(680, 520, 101, 41))
        self.checkUpdate.setFont(font)
        self.gotosavepath = QPushButton(self.centralwidget)
        self.gotosavepath.setObjectName(u"gotosavepath")
        self.gotosavepath.setGeometry(QRect(440, 350, 101, 41))
        font2 = QFont()
        font2.setFamily(u"\u9ed1\u4f53")
        font2.setPointSize(10)
        self.gotosavepath.setFont(font2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 23))
        self.menuhelp = QMenu(self.menubar)
        self.menuhelp.setObjectName(u"menuhelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuhelp.menuAction())
        self.menuhelp.addAction(self.actionhow_to_use)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"\u89c6\u9891\u683c\u5f0f\u8f6c\u6362\u5668", None))
        self.actionhow_to_use.setText(QCoreApplication.translate("MainWindow", u"\u4f7f\u7528\u65b9\u6cd5", None))
#if QT_CONFIG(tooltip)
        self.btnTransform.setToolTip(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u8f6c\u6362", None))
#endif // QT_CONFIG(tooltip)
        self.btnTransform.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u5173\u6ce8\u516c\u4f17\u53f7 Edtshare</p><p>\u89e3\u9501\u66f4\u591a\u5b9e\u7528\u5de5\u5177</p></body></html>", None))
        self.label_7.setText("")
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u4f5c\u8005:\u674e\u4e16\u8654</p><p>\u5236\u4f5c\u5de5\u5177\uff1apyside2 + python3 </p><p>\u90ae\u7bb1\uff1adevount@qq.com</p><p>\u7f51\u7ad9\uff1awww.edtshare.com</p></body></html>", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"\u8f6c\u6362\u65b9\u6cd5", None))
#if QT_CONFIG(tooltip)
        self.ToPPT.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u65e9\u671f\u7248\u672cPPT\uff082010\u7248\u4e4b\u524d\u542b2010\u7248\uff09\uff0c\u53ea\u652f\u6301\u63d2\u5165\u89c6\u9891\u683c\u5f0f\u4e3awmv\uff0c\u56e0\u6b64\u6b64\u5904\u5c06\u6e90\u89c6\u9891\u8f6c\u6362\u4e3awmv\u683c\u5f0f\u3002</p><p>\u53c2\u6570\u5982\u4e0b\uff1a\u97f3\u9891\u7f16\u7801wmav2,\u89c6\u9891\u7f16\u7801wmv2,\u5c01\u88c5\u683c\u5f0f\u4e3awmv\uff0c\u5176\u4ed6\u53c2\u6570\u4e0d\u53d8\u3002</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.ToPPT.setText(QCoreApplication.translate("MainWindow", u"\u8f6c\u6362\u4e3a\u65e9\u671fPPT\u64ad\u653e\u683c\u5f0f", None))
#if QT_CONFIG(tooltip)
        self.GetAudio.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u5c06\u89c6\u9891\u4e2d\u7684\u97f3\u9891\u8f6c\u6362\u4e3aaac\u7f16\u7801\u683c\u5f0f\uff0c\u5e76\u4fdd\u5b58\u5230\u5355\u4e2amp3\u6587\u4ef6\u4e2d</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.GetAudio.setText(QCoreApplication.translate("MainWindow", u"\u63d0\u53d6\u6587\u4ef6\u4e2d\u7684\u58f0\u97f3", None))
#if QT_CONFIG(tooltip)
        self.ToMP4.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u5c06\u6e90\u89c6\u9891\u8f6c\u6362\u4e3a\u901a\u7528mp4\u683c\u5f0f\uff0c\u97f3\u9891\u7f16\u7801\u4e3aaac\uff0c\u89c6\u9891\u7f16\u7801\u4e3ah264</p><p>\u5176\u4ed6\u7684\u53c2\u6570\u4fdd\u6301\u4e0e\u6e90\u89c6\u9891\u4e00\u81f4\u4e0d\u53d8</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.ToMP4.setText(QCoreApplication.translate("MainWindow", u"\u8f6c\u6362\u4e3a\u901a\u7528MP4\u683c\u5f0f", None))
#if QT_CONFIG(tooltip)
        self.ToCD.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u7531\u4e8e\u5149\u76d8DVD\u683c\u5f0f\u7684\u5199\u5165\u901f\u5ea6\u4e0d\u9ad8\uff0c\u5bfc\u81f4\u89c6\u9891\u7801\u7387\u8fc7\u9ad8\u65f6\u523b\u5f55\u6210DVD\u65e0\u6cd5\u62d6\u52a8\u64ad\u653e\uff0c\u56e0\u6b64\u6b64\u5904\u5c06\u5176\u7801\u7387\u8f6c\u6362\u4e3a4000kb/s\uff0c\u53c2\u6570\u5982\u4e0b\uff1a\u97f3\u9891\u7f16\u7801 aac,\u89c6\u9891\u7f16\u7801 h264,\u89c6\u9891\u7801\u73874000k</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.ToCD.setText(QCoreApplication.translate("MainWindow", u"\u538b\u7f29\u5230\u5149\u76d8\u53ef\u523b\u5f55\u683c\u5f0f", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("MainWindow", u"\u89c6\u9891\u8def\u5f84", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u6e90\u89c6\u9891\u8def\u5f84\uff1a", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa\u89c6\u9891\u8def\u5f84\uff1a", None))
#if QT_CONFIG(tooltip)
        self.SourceButton.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u70b9\u51fb\u6d4f\u89c8\uff0c\u6253\u5f00\u4e00\u4e2a\u89c6\u9891\u6587\u4ef6\u3002</p><p>\u89c6\u9891\u6587\u4ef6\u7684\u683c\u5f0f\u4e0d\u9650\u3002</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.SourceButton.setText(QCoreApplication.translate("MainWindow", u"\u6d4f\u89c8", None))
#if QT_CONFIG(tooltip)
        self.DestButton.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u9009\u62e9\u4e00\u4e2a\u8f93\u51fa\u6587\u4ef6\u4f4d\u7f6e\uff0c\u5e76\u6307\u5b9a\u6587\u4ef6\u540d\u3002</p><p>\u5f53\u8f6c\u6362\u9009\u9879\u4e3a\u201c\u8f6c\u6362\u4e3a\u901a\u7528MP4\u683c\u5f0f\u201d\u548c\u201c\u538b\u7f29\u5230\u5149\u76d8\u53ef\u523b\u5f55\u683c\u5f0f\u201d\u65f6\uff0c\u53ef\u9009\u8f93\u51fa\u6587\u4ef6\u683c\u5f0f\u4e3amp4.</p><p>\u5f53\u8f6c\u6362\u9009\u9879\u4e3a\u201c\u8f6c\u6362\u4e3a\u65e9\u671fPPT\u64ad\u653e\u683c\u5f0f\u201d\u65f6\uff0c\u53ef\u9009\u8f93\u51fa\u6587\u4ef6\u683c\u5f0f\u4e3awmv</p><p>\u5f53\u5f53\u8f6c\u6362\u9009\u9879\u4e3a\u201c\u63d0\u53d6\u6587\u4ef6\u4e2d\u7684\u58f0\u97f3\u201d\u65f6\uff0c\u53ef\u9009\u8f93\u51fa\u6587\u4ef6\u683c\u5f0f\u4e3amp3</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.DestButton.setText(QCoreApplication.translate("MainWindow", u"\u6d4f\u89c8", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("MainWindow", u"\u8f6c\u6362\u8fdb\u5ea6", None))
#if QT_CONFIG(tooltip)
        self.txtOutput.setToolTip(QCoreApplication.translate("MainWindow", u"\u663e\u793a\u8f6c\u6362\u8fdb\u5ea6\uff0c\u8f6c\u6362\u5b8c\u6210\u540e\u663e\u793a\u201c\u8f6c\u6362\u5b8c\u6210\u201d", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.checkUpdate.setToolTip(QCoreApplication.translate("MainWindow", u"\u68c0\u67e5\u66f4\u65b0", None))
#endif // QT_CONFIG(tooltip)
        self.checkUpdate.setText(QCoreApplication.translate("MainWindow", u"\u68c0\u67e5\u66f4\u65b0", None))
#if QT_CONFIG(tooltip)
        self.gotosavepath.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u6253\u5f00\u8f93\u51fa\u8def\u5f84\uff0c\u4fbf\u4e8e\u5feb\u901f\u627e\u5230\u8f6c\u6362\u540e\u7684\u89c6\u9891</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.gotosavepath.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8f93\u51fa\u4f4d\u7f6e", None))
        self.menuhelp.setTitle(QCoreApplication.translate("MainWindow", u"\u5e2e\u52a9", None))
    # retranslateUi

